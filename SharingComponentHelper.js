({
	reload: function(component) {
		const helper = this;

		const action = component.get('c.getCurrentShares');
		action.setParams({
			recordId: component.get('v.recordId')
		});
		action.setCallback(this, function(response) {
			const state = response.getState();
			if (state === 'SUCCESS') {
			    const shares = JSON.parse(response.getReturnValue());
				component.set('v.shares', shares);
			}  else if (state === 'ERROR' && component.isValid()) {
				let err = response.getError();
                component.set('v.message', err[0].message);
			}
		});
		$A.enqueueAction(action);
	},

    /**
     * Updates or inserts the given share with the given level
     *
     * @param userOrGroupId The id of the user or group for the share
     * @param level the level of access to be given
     */
	upsertShare: function(component, btn, userOrGroupId, level) {
		const helper = this;
		const action = component.get('c.upsertShare');

		action.setParams({
			userOrGroupId: userOrGroupId,
			recordId: component.get('v.recordId'),
			level: level
		});

		action.setCallback(this, function(response){
			const state = response.getState();
			const btnSuccess = 'slds-button_success';
			if (state === 'SUCCESS') {
				helper.reload(component);
				$A.util.addClass(btn, btnSuccess);
				setTimeout($A.getCallback(function() {
				    $A.util.removeClass(btn, btnSuccess);
                }), 2000);
			} else if (state === 'ERROR' && component.isValid()) {
				$A.util.addClass(btn, 'slds-button_destructive');
				let err = response.getError();
                component.set('v.message', err[0].message);
			}
		});
		$A.enqueueAction(action);
	}
