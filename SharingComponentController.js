({
    doInit: function(component, event, helper) {
       	helper.reload(component);
    },

    save: function(component, event, helper) {
        const id = component.get("v.selectedRecord").Id;
        const btn = event.getSource();
        const level = component.get("v.level");
        helper.upsertShare(component, btn, id, level);
    }
})
